//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RegisterDataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class RegisterDB
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Telnum { get; set; }
        public string Purpose { get; set; }
        public string Tomeet { get; set; }
        public string EntryDate { get; set; }
        public string Photo { get; set; }
    }
}
