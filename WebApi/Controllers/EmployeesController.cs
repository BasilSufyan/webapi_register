﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EmployeesDataAccess;
using System.Web.Http.Cors;
using System.Web.Mvc;
using System.Threading;

namespace WebApi.Controllers
{
    [EnableCorsAttribute("*","*","*")]
    public class EmployeesController : ApiController
    {
        [BasicAuthentication]
        public HttpResponseMessage Get(string gender = "all")
        {
            try
            {
                string username = Thread.CurrentPrincipal.Identity.Name;

                using (EmployeeDBEntities employee = new EmployeeDBEntities())
                {
                    switch (username.ToLower())
                    {
                        case "male":
                            return Request.CreateResponse(HttpStatusCode.OK,
                                employee.Employees.Where(e => e.Gender.ToLower() == "male").ToList());                            
                        case "female":
                            return Request.CreateResponse(HttpStatusCode.OK,
                                employee.Employees.Where(e => e.Gender.ToLower() == "female").ToList());
                          default:
                            return Request.CreateResponse(HttpStatusCode.BadRequest);
                    }
                }
            }
            catch(Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
            }
           
        }

        public HttpResponseMessage Get(int id)
        {
            using (EmployeeDBEntities employee = new EmployeeDBEntities())
            {
                Employee emp = employee.Employees.FirstOrDefault(e => e.ID == id);

                if (emp != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, emp);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Employee does not exist for the passed id");
                }
            }
        }
      
        public HttpResponseMessage Post([FromBody] Employee employee)
        {
            try
            {
                using (EmployeeDBEntities employeeDB = new EmployeeDBEntities())
                {
                    employeeDB.Employees.Add(employee);
                    employeeDB.SaveChanges();

                    var message = Request.CreateResponse(HttpStatusCode.Created, employee);
                    message.Headers.Location = new Uri(Request.RequestUri + employee.ID.ToString());
                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [DisableCors]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                using (EmployeeDBEntities employeeDB = new EmployeeDBEntities())
                {
                    Employee emp = employeeDB.Employees.FirstOrDefault(e => e.ID == id);
                    if (emp != null)
                    {
                        employeeDB.Employees.Remove(emp);
                        employeeDB.SaveChanges();
                    }
                    else
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "employee not found for the Id passed");
                    }
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        } 
    }
}
