﻿using RegisterDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace WebApi.Controllers
{
    [EnableCorsAttribute("*", "*", "*")]
    public class RegisterController : ApiController
    {
        public HttpResponseMessage GetRegisterEntries()
        {
            try
            {
                List<RegisterDB> dbobj = new List<RegisterDB>();

                using (DigitalRegisterEntities DbEntity = new DigitalRegisterEntities())
                {
                    dbobj = DbEntity.RegisterDBs.ToList();
                    Imageformatting image = new Imageformatting();
                    string filepath ;

                    for(int i=0;i< dbobj.Count;i++)
                    {
                        filepath = AppDomain.CurrentDomain.BaseDirectory + "Images\\" + dbobj[i].ID + ".jpeg";
                        dbobj[i].Photo = image.RetreiveData(filepath);
                    }
                    
                    return Request.CreateResponse(HttpStatusCode.OK, dbobj);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [Route("{id}", Name = "GetRegistryById")]
        public RegisterDB Get(int id)
        {
            try
            {
               RegisterDB dbobj = new RegisterDB();

                using (DigitalRegisterEntities DbEntity = new DigitalRegisterEntities())
                {
                    return DbEntity.RegisterDBs.FirstOrDefault(entry => entry.ID == id);                    
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public HttpResponseMessage Post([FromBody] RegisterDB newEntry)
        {
            try
            {
                using (DigitalRegisterEntities DbEntity = new DigitalRegisterEntities())
                {
                    string photoblob = newEntry.Photo;
                    string filepath = AppDomain.CurrentDomain.BaseDirectory+ "Images\\";
                    newEntry.Photo = "";
                    DbEntity.RegisterDBs.Add(newEntry);
                    DbEntity.SaveChanges();
                    filepath = filepath + newEntry.ID + ".jpeg";

                    Imageformatting image = new Imageformatting();
                    image.StoreData(filepath, photoblob); // stores image at the following location

                    var message = Request.CreateResponse(HttpStatusCode.Created, newEntry);
                    //message.Headers.Location = new Uri(Request.RequestUri + newEntry.ID.ToString());
                    message.Headers.Location = new Uri(Url.Link("GetRegistryById", new { Id = newEntry.ID }));
                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
