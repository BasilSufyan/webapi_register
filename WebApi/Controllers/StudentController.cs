﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Models;

namespace WebApi.Controllers
{
    [RoutePrefix("api/students")]
    public class StudentController : ApiController
    {
        static List<Student> students = new List<Student>()
        {
            new Student() {Id = 1, Name="Basil" },
            new Student() {Id = 2, Name="Hamza" },
            new Student() {Id = 3, Name="Anis" },
        };

        [Route("")]
        public IEnumerable<Student> Get()
        {
            return students;
        }

        [Route("{id:int:range(1,30)}",Name ="GetStudentById")]
        public Student Get(int id)
        {
            return students.FirstOrDefault(student => student.Id == id);
        }
        [Route("{name:alpha}")]
        public Student Get(string name)
        {
            return students.FirstOrDefault(student => student.Name.ToLower() == name.ToLower());
        }

        [Route("")]
        public HttpResponseMessage Post(Student student)
        {
            try
            {
                students.Add(student);
                var response = Request.CreateResponse(HttpStatusCode.Created);
                response.Headers.Location = new Uri(Url.Link("GetStudentById",new {Id = student.Id }));
                return response;

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [Route("{id}/courses")] //sttribute routing
        public IEnumerable<string> GetStudentCourses(int id)
        {
            if (id == 1)
            {
                return new List<String>() { "C#", "Asp.Net", "Javascript" };
            }
            else if (id == 2)
            {
                return new List<String>() { "Java", "Springs", "Hybernate" };
            }
            else
            {
                return new List<String>() { "node", "Angular", "React" };
            }
        }

        [Route("~/api/teachers")] //~ is used to ignore the Route Prefix added at the top
        public IEnumerable<Teacher> GetTeachers()
        {
            List<Teacher> teachers = new List<Teacher>()
            {
                new Teacher() {Id = 1, Name="wqe" },
                new Teacher() {Id = 2, Name="joe" },
                new Teacher() {Id = 3, Name="soe" },
            };
            return teachers;
        }
    }
}
